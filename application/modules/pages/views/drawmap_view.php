<div class="map-showcase drawmap-page">
	<div id="drawmap">
          <div id="drawmap-map" ></div>
          <div id="drawmap-actions">
            <div id="drawmap-button-clear" class="drawmap-button">Clear</div>
            <div id="drawmap-button-help" class="drawmap-button">Help</div>
          </div>
          <div id="drawmap-help">
            <h5>How to use this Drawmap</h5>
            <ol>
              <li>Click the map to drop your first area marker</li>
              <li>Drop further markers to draw your search area</li>
              <li>Double-click to close the search loop. Happy hunting.</li>
            </ol>
          </div>
          <div id="drawmap-loading">Searching for properties &hellip;</div>

		<div id="drawmap-button-goto" href="javascript:void(0);">
			<span id="drawmap-propcount"></span>
		</div>
	</div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?libraries=drawing&sensor=false"></script>
<script type="text/javascript" src="<?=asset('js/markerclusterer.js');?>"></script>
<script type="text/javascript" src="<?=asset('vendor/drawmapjs/js/drawmap.js');?>"></script>
<link href="<?=asset('vendor/drawmapjs/css/drawmap.css');?>" rel="stylesheet" type="text/css" />
