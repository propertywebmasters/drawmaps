# File setup

## Add a page on the database having a "drawmap" slug.

## Add the CSS and JS dependency
-->  assets\vendor\drawmapjs

## Add the drawmap_view.php file on the page view module.
--> application\modules\pages\views

## Add the drawmaps.php file page-template on the template view module.
--> application\modules\templates\views\page-templates

## application\config\routes.php
--> Append the routes.
```
$route['((\w{2})/)?drawmap'] = 'pages/drawmap';
$route['((\w{2})/)?drawmap/(:any)'] = 'pages/drawmap/$3';
```

## application\modules\pages\controllers\pages.php
--> Append the function.

```
function drawmap($mode = false){
    $modes = array('buy', 'rent');
    $mode = ($mode && in_array($mode, $modes)) ? $mode : reset($modes);

    $slug = $this->uri->segment(1); // drawmap...
    $page = $this->page_m->get_by_slug($slug);

    if ($page == FALSE) { show_404(); }
    $data['title'] = $page->title;
    $data['content'] = $page->content;

    if (isset($alert)) { $data['alert'] = modules::run('alerts/_show', $alert); }else{ $data['alert'] = ""; }
    $data['meta'] = $page->meta;
    $data['hide_sidebar'] = true;
    $data['mode'] = $mode;
    //$data['jssor_slider'] = 'drawmap';

    $data['content'] = $page->content."".$this->load->view('drawmap_view', $data, TRUE);
    echo modules::run('templates/drawmaps', $data);
}
```

## application\modules\templates\controllers\templates.php
--> Append the function.

```
function drawmaps($data = FALSE)
{
    // require subscribe form filter in everypage
    $this->submit_subscribe();

    $defaults['shortlist_count'] =  modules::run('shortlist/_get_count');
    $defaults['news_snippets']= modules::run('news/_snippet');
    $defaults['property_types'] = modules::run('property_types/_dropdown_options_slug', isset($criteria["property_type"]) ? $criteria["property_type"]: "" );
    $defaults['copyright_bottom']= modules::run('settings/_get_value', "copyright_bottom");
    $defaults['body_class'] =  'generic-page';

    $data = array_merge($defaults, $data);

    // do shortcodes
    $data["content"] = $this->shortcode_m->myshort_code($data["content"]);

    $page_type = (isset($data["page"]->page_type)) ? $data["page"]->page_type : "";

    $this->load->view('page-templates/drawmaps', $data);
 }
 ```
